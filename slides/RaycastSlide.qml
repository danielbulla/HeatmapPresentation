import QtQuick 2.4
import ".."

SteppingSlide {
    title: "Heatmap Volumenrendering"
    fontScale: 0.8
    content: [
        "Raycasting",
        " emission-absorbtion model", //vri entsteht durch emi/abs-model: terme: kappa, q spektrum energie
        " Volume-Rendering Integral", //< show it
        " diskretisiert: \nfront-to-back Rendering",
        "Datenstruktur für Volumen",
        " Sparse Voxel Octree, k-d-Baum?",
        "  für Kompression",
        "  hohe Kosten bei der Erstellung (statische Daten)",
        " 3D Textur für sichtbaren Bereich",
        "  Kompression nicht nötig",
        "  schnell"
    ]
    Image {
        id:imgVolEq
        x: parent.height*0.7
        y: parent.height*0.08
        height: parent.height*0.1
        source: "qrc:/images/render-equation.png"
        fillMode: Image.PreserveAspectFit
        smooth: true
        mipmap: false
    }
    Image {
        id:imgVolRendInt
        x: parent.height*0.7
        y: parent.height*0.18
        height: parent.height*0.15
        source: "qrc:/images/volume-rendering-integral.png"
        fillMode: Image.PreserveAspectFit
        smooth: true
        mipmap: false
    }
    Image {
        id:img
        height: parent.height
        anchors.right: parent.right
        source: "qrc:/images/voxel.svg"
        fillMode: Image.PreserveAspectFit
        verticalAlignment: Image.AlignTop
        horizontalAlignment: Image.AlignHCenter
        smooth: true
        mipmap: false
    }
//    Column {
//        height: parent.height
//        anchors.right: parent.right
//        width: parent.width*0.5
//        Image {
//            id:img2
//            height: parent.height
//            anchors.right: parent.right
//            source: "qrc:/images/spaces_worldspace.svg"
//            fillMode: Image.PreserveAspectFit
//            verticalAlignment: Image.AlignTop
//            horizontalAlignment: Image.AlignHCenter
//            smooth: true
//            mipmap: false
//        }
//        Image {
//            id:img3
//            height: parent.height
//            anchors.right: parent.right
//            source: "qrc:/images/spaces_viewspace_ortho.svg"
//            fillMode: Image.PreserveAspectFit
//            verticalAlignment: Image.AlignTop
//            horizontalAlignment: Image.AlignHCenter
//            smooth: true
//            mipmap: false
//        }
//        Image {
//            id:img4
//            height: parent.height
//            anchors.right: parent.right
//            source: "qrc:/images/spaces_viewspace_ortho_polar.svg"
//            fillMode: Image.PreserveAspectFit
//            verticalAlignment: Image.AlignTop
//            horizontalAlignment: Image.AlignHCenter
//            smooth: true
//            mipmap: false
//        }
//    }
}
