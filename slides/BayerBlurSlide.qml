import QtQuick 2.4
import ".."

SteppingSlide {
    id:theSlide
    title: "Ray Offset und Unschärfe"
    fontScale: 0.7
    delayPoints: true
    contentWidth: parent.width * 0.5
    content: [
        "Artefakte: \"Holzmaserung\" / Aliasing",
        "Verschiebung benachberter Strahlen in unterschiedliche Tiefen",
        " Bayer Matrix garantiert maximale Verschiebung zwischen Nachbarn",
        "Entfernung des Musters durch Weichzeichnung",
        " Gaußscher Weichzeichner -> separierbar, effizient [1]",
        " bilaterale Filterung -> separieren nach [2]",
        ""
    ]
    textFormat: Text.RichText

    //showNotes: true
    notes:  ""

    Image {
        id:bayerImg
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.01
        height: parent.height*0.5 //+ (parent.height*0.5 * (theSlide.currentBullet <= 2))
        source: "qrc:/images/bayer.svg"
        fillMode: Image.PreserveAspectFit
        opacity: (theSlide.currentBullet == 2)
        Behavior on height {
            NumberAnimation { duration: 500 }
        }
        Behavior on opacity {
            NumberAnimation { duration: 500 }
        }
    }
    Text {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.01
        anchors.left: parent.left
        anchors.right: imgCol.left
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        text: "[1] Daniel Rákos, \"Efficient Gaussian blur with liear sampling\", 2010\n[2] Tuan Q. Pham, Lucas J. van Vliet. \"Separable Bilateral Filtering for fast Video Processing\" IEEE International Conference, 2005\n"
        font.pixelSize: theSlide.fontSize*0.5
        color: theSlide.textColor
        visible: (theSlide.currentBullet >= theSlide.content.length-3)
    }
    Item {
        id: imgCol
        width: parent.width*0.4
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.01
        anchors.topMargin: -parent.height*0.06
        Image {
            id:firstImg
            property int mybullet: 0
            opacity: (theSlide.currentBullet >= mybullet) || (theSlide.currentBullet < firstImg.mybullet)
            width: parent.width
            height: parent.height*0.25 * (theSlide.currentBullet != mybullet) + parent.height * (theSlide.currentBullet == mybullet)
            source: "qrc:/images/raymarching-optimnothing.png"
            fillMode: Image.PreserveAspectCrop
            verticalAlignment: Image.AlignVCenter
            smooth: true
            mipmap: false
            Behavior on y {
                NumberAnimation{ duration: 500 }
            }
            Behavior on height {
                NumberAnimation{ duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation{ duration: 500 }
            }
        }
        Image {
            property int mybullet: 1
            y: (theSlide.currentBullet != mybullet) * parent.height*0.25
            opacity: (theSlide.currentBullet >= mybullet) || (theSlide.currentBullet < firstImg.mybullet)
            width: parent.width
            height: parent.height*0.25 * (theSlide.currentBullet != mybullet) + parent.height * (theSlide.currentBullet == mybullet)
            source: "qrc:/images/raymarching-optimoffset.png"
            fillMode: Image.PreserveAspectCrop
            verticalAlignment: Image.AlignVCenter
            smooth: true
            mipmap: false
            Behavior on y {
                NumberAnimation{ duration: 500 }
            }
            Behavior on height {
                NumberAnimation{ duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation{ duration: 500 }
            }
        }
        Image {
            property int mybullet: 4
            y: (theSlide.currentBullet != mybullet) * parent.height*0.5
            opacity: (theSlide.currentBullet >= mybullet) || (theSlide.currentBullet < firstImg.mybullet)
            width: parent.width
            height: parent.height*0.25 * (theSlide.currentBullet != mybullet) + parent.height * (theSlide.currentBullet == mybullet)
            source: "qrc:/images/raymarching-optimblurry.png"
            fillMode: Image.PreserveAspectCrop
            verticalAlignment: Image.AlignBottom
            smooth: true
            mipmap: false
            Behavior on y {
                NumberAnimation{ duration: 500 }
            }
            Behavior on height {
                NumberAnimation{ duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation{ duration: 500 }
            }
        }
        Image {
            property int mybullet: 5
            y: (theSlide.currentBullet != mybullet) * parent.height*0.75
            opacity: (theSlide.currentBullet >= mybullet) || (theSlide.currentBullet < firstImg.mybullet)
            width: parent.width
            height: parent.height*0.25 * (theSlide.currentBullet != mybullet) + parent.height * (theSlide.currentBullet == mybullet)
            source: "qrc:/images/raymarching-optimoffsetblur.png"
            fillMode: Image.PreserveAspectCrop
            verticalAlignment: Image.AlignBottom
            smooth: true
            mipmap: false
            Behavior on y {
                NumberAnimation{ duration: 500 }
            }
            Behavior on height {
                NumberAnimation{ duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation{ duration: 500 }
            }
        }
    }
}
