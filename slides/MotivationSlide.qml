import QtQuick 2.0
import ".."

SteppingSlide {
    id: theSlide
    delayPoints: true
    title: "Motivation"
    textFormat: Text.RichText
    content: [
        "Industrieprojekt mit Siemens AG: <i>Geo Renderer</i>",
        " Echtzeitmessdaten aus dem Stromnetz",
        " Geolokalisierbare Daten (spatial analysis)",
        "Teil dieser Arbeit:",
        " Integration von 2D Heatmaps",
        " Umsetzung mit OGRE 3D"
    ]
    _pointCounter: 2
    currentBullet: 2
    contentWidth: width*0.9-imgColumn.width
    Column {
        id:imgColumn
        anchors.right: parent.right
        height: parent.height
        spacing: parent.height*0.02
        Column {
            Image {
                id: img1
                height: imgColumn.height*0.45
                //opacity: theSlide.currentBullet > 1 || theSlide.currentStep >= 1 && theSlide.currentBullet == 1
                source: "qrc:/images/Screenshot1.png"
                fillMode: Image.PreserveAspectFit

                Behavior on opacity {
                    NumberAnimation { duration: 500 }
                }
            }
            Text {
                width: img1.width
                opacity: img1.opacity
                text: ""
                color: "white"
                horizontalAlignment: Text.AlignHCenter
            }
        }

        Column {
            Image {
                id: img2
                height: img1.height
                //opacity: theSlide.currentBullet > 1 || theSlide.currentStep >= 2 && theSlide.currentBullet == 2
                source: "qrc:/images/Screenshot3.png"
                fillMode: img1.fillMode

                Behavior on opacity {
                    NumberAnimation { duration: 500 }
                }
            }
            Text {
                width: img2.width
                opacity: img2.opacity
                text: ""
                color: "white"
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }

    function advanceStep() {
        switch(theSlide.currentBullet) {
        case 2:
            _pointCounter = content.length-1;
            currentBullet = content.length-1;
            return true;
        default:
            return false;
        }
    }
}
