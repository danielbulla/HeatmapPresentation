import QtQuick 2.0
import ".."

SteppingSlide {
    id: theSlide
    delayPoints: true
    title: "Motivation"
    textFormat: Text.RichText
    fontScale: 0.9
    content: [
        "Analyse von Nutzerverhalten",
        " Datenanalyse in 2D",
        " Datenanalyse in 3D",
        "Teil dieser Arbeit:",
        " 2D: OpenGL 3.3 Prototyp",
        " 3D: OpenGL 4.3 Prototyp",
        " Integration in moderne Render- pipeline (deferred renderer)"
    ]
    contentWidth: width*0.46

    Image {
        z: img2.z+1
        id: imgVol3d
        anchors.bottom: captionImgVol3d.top
        anchors.right: parent.right
        anchors.rightMargin: parent.width*0.26// - parent.width * 0.3 * (theSlide.currentBullet > 3 || theSlide.currentBullet <= 1)
        height: parent.height * 0.6 - parent.height * 0.15 * (theSlide.currentBullet > 3 || theSlide.currentBullet <= 0)
        width: height*2.0
        opacity: theSlide.currentBullet >= 2
        source: "qrc:/images/set5-freeinsponza.png"
        fillMode: Image.PreserveAspectFit
        horizontalAlignment: Image.AlignRight

        Behavior on opacity {
            NumberAnimation { duration: 500 }
        }
        Behavior on anchors.rightMargin {
            NumberAnimation { duration: 500 }
        }
        Behavior on height {
            NumberAnimation { duration: 500 }
        }
    }
    Text {
        id:captionImgVol3d
        anchors {
            bottom: parent.bottom
            left: imgVol3d.left
            right: imgVol3d.right
            bottomMargin: parent.height*0.01
        }
        opacity: imgVol3d.opacity
        text: ""
        color: "white"
        horizontalAlignment: Text.AlignHCenter
    }

    Image {
        id: img1
        anchors.right: parent.right
        anchors.top: parent.top
        height: parent.height*0.45 + (theSlide.currentBullet === 0 && theSlide === thePresentation.slides[thePresentation.currentSlide]) * parent.height*0.2
        width: height
        opacity: theSlide.currentBullet >= 0 // && (theSlide.currentBullet <= 3)
        source: "qrc:/images/m-et-heatmap-usabilityde.jpg"//35031_super_mario_bros-min-rect.jpg"
        fillMode: Image.PreserveAspectFit
        horizontalAlignment: Image.AlignRight

        Behavior on opacity {
            NumberAnimation { duration: 500 }
        }
        Behavior on height {
            enabled: img1.parent.height != 0
            NumberAnimation { duration: 500 }
        }
    }
    Text {
        anchors {
            top: img1.bottom
            left: img1.left
            right: img1.right
        }
        opacity: img1.opacity
        text: "Quelle: usability.de"
        color: "white"
        horizontalAlignment: Text.AlignHCenter
    }
    Image {
        id: img2
        anchors.right: parent.right
        anchors.bottom: captionImg2.top
        height: img1.height + (theSlide.currentBullet <= 1 && theSlide.currentBullet != 0) * parent.height*0.2
        opacity: theSlide.currentBullet >= 1 // && (theSlide.currentBullet <= 3)
        source: "qrc:/images/TF2Heatmap.png"
        fillMode: img1.fillMode

        Behavior on opacity {
            NumberAnimation { duration: 500 }
        }
        Behavior on height {
            NumberAnimation { duration: 500 }
        }
    }
    Text {
        id:captionImg2
        anchors {
            bottom: parent.bottom
            bottomMargin: parent.height*0.01
            left: img2.left
            right: img2.right
        }
        opacity: img2.opacity
        text: "Quelle: memoryleakinterviews.com"
        color: "white"
        horizontalAlignment: Text.AlignHCenter
    }
    function advanceStep() {
        switch(theSlide.currentBullet) {
//        case 0:
//            return theSlide.currentStep < 1;
//        case 1:
//            return theSlide.currentStep < 1;
        case 2:
            _pointCounter = content.length-1;
            currentBullet = content.length-1;
            return true;
        default:
            return false;
        }
    }
}
