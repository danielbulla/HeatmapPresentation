import QtQuick 2.4
import ".."

SteppingSlide {
    id: theSlide
    delayPoints: true
    showAllPoints: true
    title: "Niveaulinien"
    fontScale: 0.7
    contentWidth: width*0.5
    content: [
        "CONREC - A Contouring Subroutine, Paul Bourke, 1987",
        " ähnelt marching Squares",
        " 5 Abtastpunkte pro Zelle",
        " Erzeugung einer Kontur als Polygonzug",
        "Rasterbasierter Ansatz:",
        " Rechenleistung für viele Zellen (eine pro Pixel)",
        " 1 Abtastpunkt + Betrag des Gradienten",
        " Polygonzug nicht notwendig (Pixelperfekt)" //Note: Stattdessen: Beantwortung der Frage: befindet sich der Pixel/die Zelle auf der Linie
    ]

//    property list<HeatmapDatapointModel> pointModel: [
//        HeatmapDatapointModel { centerx: 0.300; centery: 0.200 },
//        HeatmapDatapointModel { centerx: 0.100; centery: 0.300; radius: 0.50 },
//        HeatmapDatapointModel { centerx: 0.500; centery: 0.300 },
//        HeatmapDatapointModel { centerx: 0.700; centery: 0.400; radius: 0.100 },
//        HeatmapDatapointModel { centerx: 0.777; centery: 0.200; radius: 0.120 },
//        HeatmapDatapointModel { centerx: 0.800; centery: 0.300; radius: 0.300 },
//        HeatmapDatapointModel { centerx: 0.717; centery: 0.240; radius: 0.80 }
//    ]

    Rectangle {
        height: parent.height*0.99
        width: parent.width*0.5
        anchors.right: parent.right
        opacity: 1.0 - theEffect.opacity
//        Heatmap2D {
//            anchors.fill: parent
//            points: theSlide.pointModel
//            angleHoriz: 0.0
//            asX: false
//            withRad: false
//            visDP: true
//            colorlookup: true
//            contours: true
//        }
        Image {
            anchors.fill: parent
            source: "qrc:/images/hm2dpersp.png"
            fillMode: Image.PreserveAspectCrop
            smooth: true
            mipmap: true
            opacity: theSlide.currentStep <= 1
        }
        Image {
            anchors.fill: parent
            source: "qrc:/images/hm2dpersp-zoom.png"
            fillMode: Image.PreserveAspectCrop
            smooth: true
            mipmap: true
            opacity: theSlide.currentStep === 2
        }
        Image {
            anchors.fill: parent
            source: "qrc:/images/contour.png"
            fillMode: Image.PreserveAspectFit
            smooth: true
            mipmap: false
            opacity: theSlide.currentStep === 3
        }
        Image {
            anchors.fill: parent
            source: "qrc:/images/contour2.png"
            fillMode: Image.PreserveAspectFit
            smooth: true
            mipmap: false
            opacity: theSlide.currentStep > 3
        }
    }
    ShaderEffect {
        id:theEffect
        height: parent.height*0.99
        width: parent.width*0.5
        anchors.right: parent.right

        property variant src: imgSrc
        property real scale: 1.0 + (theSlide.currentStep >= 1) * 50.0
        opacity: 1.0 - (theSlide.currentStep >= 1)
        vertexShader: "
            uniform highp mat4 qt_Matrix;
            uniform highp float scale;
            attribute highp vec4 qt_Vertex;
            attribute highp vec2 qt_MultiTexCoord0;
            varying highp vec2 coord;
            void main() {
                if(scale < 25.0) { scale = floor(scale); }
                coord = qt_MultiTexCoord0 * scale;
                gl_Position = qt_Matrix * qt_Vertex;
            }"
        fragmentShader: "
            varying highp vec2 coord;
            uniform sampler2D src;
            uniform lowp float qt_Opacity;
            uniform highp float scale;
            void main() {
                lowp vec4 tex = texture2D(src, coord);
                //lowp float unsat = min(1.0, (scale/50.0));
                //tex.rgb *= 1.0-unsat;
                //tex.rgb += unsat*0.5;
                gl_FragColor = tex * qt_Opacity;
            }"
        Behavior on scale {
            enabled: theEffect.scale == 1.0
            NumberAnimation {
                duration: 15000
                easing.type: Easing.InQuad
            }
        }
        Behavior on opacity {
            enabled: theEffect.scale <= 2.0
            NumberAnimation {
                duration: 15000
                easing.type: Easing.InQuad
            }
        }
    }

    ShaderEffectSource {
        id: imgSrc
        sourceItem: img
        hideSource: true
        wrapMode: ShaderEffectSource.Repeat
    }

    Image {
        id:img
        height: parent.height*0.99// * (1.5-theSlide.currentStep)
        width: parent.width*0.5
        anchors.right: parent.right
        source: "qrc:/images/CONREC-inv.svg"
        fillMode: Image.PreserveAspectFit
        verticalAlignment: Image.AlignTop
        horizontalAlignment: Image.AlignHCenter
        smooth: true
        mipmap: true
    }

    function advanceStep() {
        return currentStep < 4;
    }
}
