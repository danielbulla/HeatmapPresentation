import QtQuick 2.4
import ".."

SteppingSlide {
    title: "Voxel und Raycasting Koordinatensystem"
//    fontScale: 0.9
//    content: [
//        "View -> 3D Textur",
//        " (1) world: (x,y,z) -> inv(mvp) * (x,y,z, 1)",
//        " (2) view:  (x,y,z) -> inv(p) * (x,y,z, 1)",
//        " (3) polar: (x,y,z) -> inv(p) * (x,y,z, 1)); z = sqrt(x²+y²+z²))"
//    ]
    Image {
        width: parent.width*0.33
        height: parent.height*0.66
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        source: "qrc:/images/voxel.svg"
        fillMode: Image.PreserveAspectFit
        smooth: true
        mipmap: false
    }
    Column {
        anchors.fill: parent
        anchors.topMargin: -parent.height*0.06
        Rectangle {
            height: parent.height*0.33
            width: parent.width
            Grid {
                anchors.fill: parent
                anchors.leftMargin: parent.width*0.01
                anchors.rightMargin: parent.width*0.01
                columns: 3
                rows: 1
                Image {
                    width: parent.width*0.33
                    height: parent.height
                    source: "qrc:/images/spaces_worldspace.svg"
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    mipmap: false
                }
                Image {
                    width: parent.width*0.33
                    height: parent.height
                    source: "qrc:/images/spaces_viewspace_ortho.svg"
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    mipmap: false
                }
                Image {
                    width: parent.width*0.33
                    height: parent.height
                    source: "qrc:/images/spaces_viewspace_ortho_polar.svg"
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    mipmap: false
                }
            }
        }
        Grid {
            height: parent.height*0.33
            width: parent.width
            columns: 3
            rows: 1
            Image {
                width: parent.width*0.33
                height: parent.height
                source: "qrc:/images/spaces_worldspace.svg"
                fillMode: Image.PreserveAspectFit
                smooth: true
                mipmap: false
                opacity: 0.0
            }
            Image {
                width: parent.width*0.33
                height: parent.height
                source: "qrc:/images/space-homogenquads.png"
                fillMode: Image.PreserveAspectFit
                smooth: true
                mipmap: false
            }
            Image {
                width: parent.width*0.33
                height: parent.height
                source: "qrc:/images/space-sphericalquads.png"
                fillMode: Image.PreserveAspectFit
                smooth: true
                mipmap: false
            }
        }
        Grid {
            height: parent.height*0.33
            width: parent.width
            columns: 3
            rows: 1
            Image {
                width: parent.width*0.33
                height: parent.height
                source: "qrc:/images/spaces_worldspace.svg"
                fillMode: Image.PreserveAspectFit
                smooth: true
                mipmap: false
                opacity: 0.0
            }
            Image {
                width: parent.width*0.33
                height: parent.height
                source: "qrc:/images/space-homogenquadscirc.png"
                fillMode: Image.PreserveAspectFit
                smooth: true
                mipmap: false
            }
            Image {
                width: parent.width*0.33
                height: parent.height
                source: "qrc:/images/space-sphericalquadscirc.png"
                fillMode: Image.PreserveAspectFit
                smooth: true
                mipmap: false
            }
        }
    }
}
