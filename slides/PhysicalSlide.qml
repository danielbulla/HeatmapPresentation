import QtQuick 2.4
import ".."

SteppingSlide {
    id: theSlide
    title: "Physikalische Repräsentation von 3D Heatmaps"
    showBullets: false
    anchors.leftMargin: -width*0.1
    fontScale: 0.8
    delayPoints: true
    content: [
        "Diagramm, kein \nphysikalisches Phänomen",
        "1. \"emmision absorbtion model\"",
        " - sichtbares Spektrum, Licht", //Post interpolative classification. (verwaschen)
        " - Wärmestrahlung", //Post post classification. Keine Transferfunktion, bis zum 2dimensionalen Bild. (kräftige Farben)
        //Beide Ansätze machen es schwer in den Kern der Heatmaps zu schauen
        "2. Maximum Intensity", // look-thru
        "3. Kombination: Lokale Maxima",
        ""
    ]
    Item {
        width: parent.width*0.5
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.01
        anchors.topMargin: -parent.height*0.06
        Image {
            property int mybullet: 2
            opacity: (theSlide.currentBullet >= mybullet) || (theSlide.currentBullet <= 1)
            width: parent.width
            height: parent.height*0.25 * (theSlide.currentBullet != mybullet) + parent.height * (theSlide.currentBullet == mybullet)
            source: "qrc:/images/comp-color.png"
            fillMode: Image.PreserveAspectCrop
            smooth: true
            mipmap: false
            Behavior on y {
                NumberAnimation{ duration: 500 }
            }
            Behavior on height {
                NumberAnimation{ duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation{ duration: 500 }
            }
        }
        Image {
            property int mybullet: 3
            y: (theSlide.currentBullet != mybullet) * parent.height*0.25
            opacity: (theSlide.currentBullet >= mybullet) || (theSlide.currentBullet <= 1)
            width: parent.width
            height: parent.height*0.25 * (theSlide.currentBullet != mybullet) + parent.height * (theSlide.currentBullet == mybullet)
            source: "qrc:/images/comp-heat.png"
            fillMode: Image.PreserveAspectCrop
            smooth: true
            mipmap: false
            Behavior on y {
                NumberAnimation{ duration: 500 }
            }
            Behavior on height {
                NumberAnimation{ duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation{ duration: 500 }
            }
        }
        Image {
            property int mybullet: 4
            y: (theSlide.currentBullet != mybullet) * parent.height*0.5
            opacity: (theSlide.currentBullet >= mybullet) || (theSlide.currentBullet <= 1)
            width: parent.width
            height: parent.height*0.25 * (theSlide.currentBullet != mybullet) + parent.height * (theSlide.currentBullet == mybullet)
            source: "qrc:/images/comp-maxint.png"
            fillMode: Image.PreserveAspectCrop
            smooth: true
            mipmap: false
            Behavior on y {
                NumberAnimation{ duration: 500 }
            }
            Behavior on height {
                NumberAnimation{ duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation{ duration: 500 }
            }
        }
        Image {
            property int mybullet: 5
            y: (theSlide.currentBullet != mybullet) * parent.height*0.75
            opacity: (theSlide.currentBullet >= mybullet) || (theSlide.currentBullet <= 1)
            width: parent.width
            height: parent.height*0.25 * (theSlide.currentBullet != mybullet) + parent.height * (theSlide.currentBullet == mybullet)
            source: "qrc:/images/comp-localmax3.png"
            fillMode: Image.PreserveAspectCrop
            smooth: true
            mipmap: false
            Behavior on y {
                NumberAnimation{ duration: 500 }
            }
            Behavior on height {
                NumberAnimation{ duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation{ duration: 500 }
            }
        }
    }
}
