import QtQuick 2.0
import ".."

SteppingSlide {
    id: theSlide
    delayPoints: true
    title: "Algorithmus für 2D Heatmaps (Überblick)"
    contentWidth: parent.width*0.55
    fontScale: 0.8
    content: [
        "5. gemeinsame Speicherung/Verrechnung in 3D Textur",
        "6. Visualisierung durch",
        " Licht",
        " Wärmestrahlung",
        "7. Nachbearbeitung (Weichzeichner)"
    ]

    property real intensFac: 1.0//0.1+0.5*(hm.colorlookup)
    property list<HeatmapDatapointModel> pointModel: [
        HeatmapDatapointModel { centerx: 0.300; centery: 0.200; intensity: intensFac; radius: 0.1 },
        HeatmapDatapointModel { centerx: 0.010; centery: 0.300; intensity: intensFac; radius: 0.50 },
        HeatmapDatapointModel { centerx: 0.500; centery: 0.300; intensity: intensFac; radius: 0.3 },
        HeatmapDatapointModel { centerx: 0.700; centery: 0.700; intensity: intensFac; radius: 0.100 },
        HeatmapDatapointModel { centerx: 0.777; centery: 0.800; intensity: intensFac; radius: 0.120 },
        HeatmapDatapointModel { centerx: 0.800; centery: 0.900; intensity: intensFac; radius: 0.300 },
        HeatmapDatapointModel { centerx: 0.300; centery: 0.830; intensity: intensFac*0.3; radius: 0.300 },
        HeatmapDatapointModel { centerx: 0.320; centery: 0.650; intensity: intensFac*0.7; radius: 0.300 },
        HeatmapDatapointModel { centerx: 0.300; centery: 0.570; intensity: intensFac*0.5; radius: 0.100 },
        HeatmapDatapointModel { centerx: 0.717; centery: 0.240; intensity: intensFac*0.7; radius: 0.80 }
    ]

    Heatmap3D {
        id:hm
        slices: colorlookup? 64 : 16
        anchors.right: parent.right
        anchors.bottom: capHm.top
        width: parent.width * 0.36
        height: width
        points: pointModel
        angleHoriz: 0.5
        colorlookup: theSlide.currentBullet >= 2
        colorlookupPost: false
        //asRect: theSlide.currentBullet == 1
        //lowerAlpha: 0.2 + 0.8 * (1.0-(theSlide.currentBullet == 2))
        intensityFactor: 1.0//0.2+0.8*(colorlookup)
        lowerAlpha: 0.3
        //circleAdapt: theSlide.currentBullet == 2
        //circleAdapt: true
        //asRect: true
    }
    Text {
        id:capHm
        font.pixelSize: theSlide.fontSize*0.3
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.01
        anchors.left: hm.left
        anchors.right: hm.right
        horizontalAlignment: Text.AlignHCenter
        text: "Farbe (post-interpolative classification)"
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        height: implicitHeight
        color: "white"
        visible: hm.colorlookup
    }

    Heatmap3D {
        id:hm2
        slices: 64
        anchors.right: hm.left
        anchors.bottom: capHm2.top
        width: parent.width * 0.36
        height: width
        points: pointModel
        angleHoriz: 0.5
        colorlookup: false
        colorlookupPost: true
        //lowerAlpha: 0.5 //+ 0.8 * (1.0-(theSlide.currentBullet == 2))
        intensityFactor: 0.05
        lowerAlpha: 18.0
        visible: theSlide.currentBullet >= 3
    }
    Text {
        id:capHm2
        font.pixelSize: theSlide.fontSize*0.3
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.01
        anchors.left: hm2.left
        anchors.right: hm2.right
        horizontalAlignment: Text.AlignHCenter
        text: "Wärme (2D postprocessing Effekt auf Wärmebild)"
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        height: implicitHeight
        color: "white"
        visible: hm2.visible
    }

    NumberAnimation {
        id: rotationAnimation
        loops: Animation.Infinite
        target: hm
        property: "angle"
        duration: 10000
        from: 0.0
        to: Math.PI*2.0
        running: true
    }
    NumberAnimation {
        id: rotationAnimation2
        loops: Animation.Infinite
        target: hm2
        property: "angle"
        duration: 10000
        from: 0.0
        to: Math.PI*2.0
        running: true
    }
}

