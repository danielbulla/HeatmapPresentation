import QtQuick 2.3
import ".."

SteppingSlide {
    id: theSlide
    delayPoints: true
    title: "OpenGL Grafik Pipeline"
    property bool zoom: true
    function advanceStep() {
        return currentStep < 1 && zoom;
    }

    Rectangle {
        id: clipRect
        z: graphImg.z+1
        anchors.fill: parent
        anchors.topMargin: graphImg.state === "overview" ? -parent.height*0.09 : 0
        anchors.bottomMargin: graphImg.state === "overview" ? -parent.height*0.01 : parent.height*0.01
        gradient: Gradient {
            GradientStop { position: 0.0; color: "black" }
            GradientStop { position: 0.1; color: "transparent" }
            GradientStop { position: 0.9; color: "transparent" }
            GradientStop { position: 1.0; color: "black" }
        }
        Behavior on opacity {
            NumberAnimation { duration: 500 }
        }
        Behavior on anchors.topMargin {
            NumberAnimation { duration: 500 }
        }
        Behavior on anchors.bottomMargin {
            NumberAnimation { duration: 500 }
        }
    }
    Flickable {
        id: parentRect
        anchors.top: parent.top
        anchors.topMargin: clipRect.anchors.topMargin
        anchors.bottomMargin: clipRect.anchors.bottomMargin
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width*0.4
        contentHeight: graphImg.height
        //color: "transparent"
        clip:true
        Image {
            id:graphImg
            height: parent.height
            anchors.right: parent.right
            anchors.left: parent.left
            source: "qrc:/images/openglpipeline_black.png"
            fillMode: Image.PreserveAspectFit
            verticalAlignment: Image.AlignTop
            horizontalAlignment: Image.AlignHCenter
            smooth: true
            mipmap: false//theSlide.currentStep == 0
            Behavior on height {
                enabled: theSlide.currentStep > 0
                NumberAnimation { duration: 1500 }
            }
            Behavior on y {
                enabled: theSlide.currentStep > 0
                NumberAnimation { duration: 1500 }
            }

            states: [
                State {
                    id:overviewState
                    name: "overview"
                    when: theSlide.currentStep == 0
                    PropertyChanges {
                        target: graphImg
                        height: parentRect.height
                        y: 0
                    }
                    PropertyChanges {
                        target: clipRect
                        opacity: 0.0
                    }
                },
                State {
                    name: "zoomed"
                    when: theSlide.currentStep == 1
                    PropertyChanges {
                        target: graphImg
                        height: parentRect.height*2.0 //big enough to make width restrict the image
                        y: parentRect.width*0.1
                    }
                    PropertyChanges {
                        target: parentRect
                        width: theSlide.width*0.6
                    }
                    PropertyChanges {
                        target: clipRect
                        opacity: 1.0
                    }
                }
//                , State {
//                    name: "step2"
//                    when: theSlide.currentStep == 2
//                    PropertyChanges {
//                        target: graphImg
//                        height: parentRect.height*2.0
//                        anchors.rightMargin: parentRect.width*0.2
//                        anchors.leftMargin: parentRect.width*0.2
//                        y: -parentRect.width*0.2
//                    }
//                }, State {
//                    name: "step2"
//                    when: theSlide.currentStep == 2
//                    PropertyChanges {
//                        target: graphImg
//                        height: parentRect.height*2.0
//                        anchors.rightMargin: parentRect.width*0.2
//                        anchors.leftMargin: parentRect.width*0.2
//                        y: -parentRect.width*0.5
//                    }
//                }
            ]
        }
    }
}
