import QtQuick 2.3
import QtQuick.Layouts 1.1
import ".."

SteppingSlide {
    id: theSlide
    delayPoints: true
    title: "2D Heatmap - Eingabeformat"
    contentWidth: parent.width*0.55
    fontScale: 0.8
    content: [
        "Rechtecke -> continous value",
        "Rasterizer",
        " interpoliere Positionsvektor",
        "Fragmentshader",
        " Normalverteilung auswerten",
        "per Fragment Operation",
        " additives Blending"
    ]

    function advanceStep() {
        switch(currentBullet) {
        default:
            return false;
        }
    }

    CodeBlock {
        y: theSlide.height*0.45
        anchors.right: theSlide.right
        width: Math.min(theSlide.width*0.55,height*2.04)
        height: theSlide.height*0.54
        visible: theSlide.currentBullet >= 6 && theSlide.currentStep >= 1
        title: "heatmapdatapoint.frag / GLSL"
        code:
"
"
    }

    Image {
        visible: theSlide.currentBullet >= 6
        y: -theSlide.height*0.06
        anchors.right: theSlide.right
        width: theSlide.width*0.6
        height: theSlide.height*0.49
        horizontalAlignment: Image.AlignRight
        source: "qrc:/images/vertexformat-vert.png"
        fillMode: Image.PreserveAspectFit
        smooth: true
        mipmap: false
    }

    property list<HeatmapDatapointModel> pointModel: [
        HeatmapDatapointModel { centerx: 0.310; centery: 0.200; radius: 0.231},
        HeatmapDatapointModel { centerx: 0.090; centery: 0.310; radius: 0.253 },
        HeatmapDatapointModel { centerx: 0.450; centery: 0.630; radius: 0.1211 },
        HeatmapDatapointModel { centerx: 0.700; centery: 0.300; radius: 0.110 },
        HeatmapDatapointModel { centerx: 0.777; centery: 0.900; radius: 0.120 },
        HeatmapDatapointModel { centerx: 0.800; centery: 0.300; radius: 0.300 },
        HeatmapDatapointModel { centerx: 0.717; centery: 0.940; radius: 0.080 }
    ]

    Row {
        visible: theSlide.currentBullet >= 1
        spacing: theSlide.width * 0.01
        height: theSlide.height * 0.3
        anchors.bottom: theSlide.bottom
        anchors.bottomMargin: theSlide.height*0.01
        //anchors.horizontalCenter: theSlide.horizontalCenter
        anchors.left: theSlide.left

        property Gradient backgrgr: Gradient {
                                        GradientStop {
                                            position: 0.0
                                            color: Qt.rgba(0.1,0.1,0.1,1.0)
                                        }
                                        GradientStop {
                                            position: 1.0
                                            color: Qt.rgba(0.3,0.3,0.3,1.0)
                                        }
                                    }
        Rectangle {
            height: parent.height
            width: parent.height
            radius: 10
            border.color: Qt.rgba(0, 177, 172)
            border.width: 1
            gradient: parent.backgrgr
            Heatmap2D {
                anchors.fill: parent
                points: theSlide.pointModel
                angleHoriz: 0.0
                asX: true
                visDP: false
            }
        }
        Text {
            height: parent.height
            verticalAlignment: Text.AlignVCenter
            text: "=>"
            color: "white"
            font.pixelSize: Math.max(theSlide.fontSize, 1.0)
        }
        Rectangle {
            height: parent.height
            width: parent.height
            radius: 10
            border.color: Qt.rgba(0, 177, 172)
            border.width: 1
            gradient: parent.backgrgr
            Heatmap2D {
                anchors.fill: parent
                points: theSlide.pointModel
                angleHoriz: 0.0
                asX: false
                withRad: false
                visDP: false
                asRect: true
            }
        }
    }
}
