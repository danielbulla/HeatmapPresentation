import QtQuick 2.0
import ".."

SteppingSlide {

    title: "Einführung"
    content: [
        "Spezialisierung auf Eingabedaten",
        "Herleitung eines <i>continous value</i>"
    ]
    delayPoints: true
    showAllPoints: true
    textFormat: Text.RichText
    //showNotes: true
    notes:  "Meistens passt die Bezeichnung Ereignisse. (Messdaten an einem Ort, zu einem Zeitpunkt, )
Herleitung von Zwischenwerten oder gleitender Wert"

    Image {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.01
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width*0.7
        source: "qrc:/images/DataGraph-eq.png"
        fillMode: Image.PreserveAspectFit
        opacity: currentStep === 1
    }
    Image {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.01
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width*0.7
        source: "qrc:/images/DataGraph.png"
        fillMode: Image.PreserveAspectFit
    }
    function advanceStep() {
        return currentStep < 1
    }
}
