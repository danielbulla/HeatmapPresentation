import QtQuick 2.0
import ".."

SteppingSlide {
    id: hmdpSilde
    delayPoints: true
    title: "3D Voxelisierung"
    contentWidth: parent.width*0.55
    content: [
        "Geometrische Repräsentation eines 3D Datenpunkt",
        " 2D Rasterisierung",
        " Manualle Rasterisierung der dritten Dimension",
        " Normalverteilung um den Mittelpunkt im Raum",
        " Optimierung: Ränder (niedrige Werte) entfernen",
        "Rendering",
        " Transferfunktion (Farbbasiert)",
    ]

    Rectangle {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 5
        width: 16
        height: 16
        color: "grey"
        opacity: 0.1
        MouseArea {
            anchors.fill: parent
            onClicked: {
                resetSteps();
            }
        }
    }

    //0: overview:  circ: off, norm: on, zebra: off
    //1: 2d raster: circ: off, norm: off, zebra: on
    HeatmapDatapoint {
        id: hmdp
        anchors.right: parent.right
        anchors.margins: parent.width*0.05
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width*0.3
        height: width
        zebra: hmdpSilde._pointCounter > 1 && hmdpSilde._pointCounter < 3
        circleAdapt: hmdpSilde._pointCounter > 3
        showNormDist: hmdpSilde._pointCounter != 2
        outerAlpha: hmdpSilde._pointCounter > 4
        slices: 8
        sliceFrom: 0
        sliceTo: 8
        angle: Math.PI*0.25
        angleHoriz: 0.5
        colorlookup: hmdpSilde._pointCounter > 5
    }

    ParallelAnimation {
        running: hmdpSilde._pointCounter == 1
        NumberAnimation {
            target: hmdp
            property: "sliceFrom"
            duration: 0
            from: 0
            to: hmdp.slices*0.5
        }
        NumberAnimation {
            target: hmdp
            property: "sliceTo"
            duration: 0
            from: hmdp.slices
            to: hmdp.slices*0.5
        }
        NumberAnimation {
            target: hmdp
            property: "angle"
            duration: 1000
            from: Math.PI*0.25
            to: Math.PI*0.5
        }
        NumberAnimation {
            target: hmdp
            property: "angleHoriz"
            duration: 1000
            from: 0.5
            to: 0.0
        }
    }
    SequentialAnimation {
        running: hmdpSilde._pointCounter == 2
        ParallelAnimation {
            NumberAnimation {
                target: hmdp
                property: "angle"
                duration: 1000
                from: Math.PI*0.5
                to: Math.PI*0.25
            }
            NumberAnimation {
                target: hmdp
                property: "angleHoriz"
                duration: 1000
                from: 0.0
                to: 0.5
            }
        }
        ParallelAnimation {
            NumberAnimation {
                target: hmdp
                property: "sliceFrom"
                duration: 10000
                from: hmdp.slices*0.5
                to: 0
            }
            NumberAnimation {
                target: hmdp
                property: "sliceTo"
                duration: 10000
                from: hmdp.slices*0.5
                to: hmdp.slices
            }
        }
    }
    NumberAnimation {
        running: hmdpSilde._pointCounter > 2
        loops: Animation.Infinite
        target: hmdp
        property: "angle"
        duration: 10000
        from: Math.PI*0.25
        to: Math.PI*1.25
    }
}
