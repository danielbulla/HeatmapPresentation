import QtQuick 2.0
import ".."

SteppingSlide {
    id:slide
    title: "Parallelisierung"
    fontScale: 0.7

    delayPoints: true
    function advanceStep() {
        return currentStep < 2;
    }

    ContentColumn {
        width: slide.width * 0.5
        height: slide.height
        content: [
            "CPU (Beispiel)",
            " Parallelisierung anhand von Eingabedaten",
            "  Für jeden Punkt der Punktewolke ein Thread",
            "  Sequenzielle Rasterisierung",
            " Speicherzugriff",
            "  Addition in globalem Speicher",
            "  nicht atomar -> locking",
            "  Unvorhersehbar"
        ]
    }

    ContentColumn {
        visible: slide.currentStep == 2
        x: slide.width * 0.5
        width: slide.width * 0.5
        height: slide.height
        content: [
            "GPU (Grafikpipeline)",
            " Parallelisierung anhand von Eingabedaten",
            "  Vertices",
            // Dann erfolgt die " Rasterisierung",
            " Parallelisierung anhand von Ausgabedaten",
            "  Fragments / Pixel",
            " Speicherzugriff (Framebuffer)",
            "  Raster Oparation Processor (ROP)",
            "  Addition <i> in Memory <i> ",
            "  \"blending\""
        ]
    }

    CodeBlock {
        visible: slide.currentStep >= 1
        x: 0
        y: slide.height * 0.7
        width: slide.width * 0.5
        height: slide.height*0.26
        title: "Schleifen CPU / Pseudocode"
        code:
"
for each( HeatmapDatenpunkt p )
  for each( p.x )
    for each( p.y )
      memory[p.x][p.y] = memory[p.x][p.y]
                 + normalverteilung(...)"
    }
    //Notes: äußerste Schleife parallelisieren. Bei den beiden anderen werden unsere teuren CPU-Threads zu klein...
}
