import QtQuick 2.0
import QtQuick.Layouts 1.1
import ".."
import "../qtpresentation"

SteppingSlide {
    id: slideTitle
    anchors.fill: parent
    anchors.margins: parent.height * 0.05
    anchors.bottomMargin: parent.height * 0.10
    Text {
        x: parent.width * 0.01
        y: parent.height * 0.01
        width: parent.width * 0.8
        verticalAlignment: Text.AlignTop
        wrapMode: Text.WordWrap
        text: qsTr("Fachbereich 5 Elektrotechnik und Informationstechnik Fachhochschule Aachen")
        color: "white"
        font.pointSize: slideTitle.fontSize * 0.4
        font.family: "Verdana"
    }

    Column {
        x: parent.width * 0.05
        y: parent.height * 0.3 + innerText.implicitHeight*0.5
        width: parent.width * 0.8
        spacing: parent.height * 0.05

        Text {
            id: titleText
            width: parent.width
            verticalAlignment: Text.AlignTop
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            text: qsTr("Effiziente Erzeugung und Visualisierung von Heatmaps zur Analyse von Geoinformationsdaten in einem 3D Renderer")
            color: "white"
            font.pointSize: slideTitle.fontSize * 0.7
            font.family: "Verdana"
            font.capitalization: Font.AllUppercase //SmallCaps
        }
        Text {
            width: parent.width
            verticalAlignment: Text.AlignTop
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            text: qsTr("Masterarbeit")
            color: "white"
            font.pointSize: slideTitle.fontSize * 0.3
            font.family: "Verdana"
            //font.capitalization: Font.AllUppercase //SmallCaps
        }
        Text {
            width: parent.width
            verticalAlignment: Text.AlignTop
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            text: qsTr("Daniel Bulla\nMatrikel-Nummer 826708")
            color: "white"
            font.pointSize: slideTitle.fontSize * 0.3
            font.family: "Verdana"
            //font.capitalization: Font.AllUppercase //SmallCaps
        }
    }
    Rectangle {
        color: "transparent"
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: parent.height * 0.02
        height: parent.height*0.3
        width: innerText.implicitWidth //parent.width*0.4
        Text {
            id:innerText
            anchors.fill: parent
            verticalAlignment: Text.AlignBottom
            horizontalAlignment: Text.AlignLeft
            wrapMode: Text.NoWrap
            text: qsTr( "Betreuer Prof. Dipl.-Inf. Ingrid Scholl\n" +
                        "Erstprüfer Prof. Dipl.-Inf. Ingrid Scholl\n" +
                        "Zweitprüfer M.Sc. Stephan Rohmen")
            color: "white"
            font.pointSize: slideTitle.fontSize * 0.5
            font.family: "Verdana"
            //font.capitalization: Font.AllUppercase //SmallCaps
        }
    }
}
