import QtQuick 2.0
import ".."

//Vorher: Grafikpipeline! Shader?! Bei jedem Schritt vorabnehmen, wann wir es verwenden werden
//Vorher: Betrachtung zu Niveaulinien
SteppingSlide {
    id: theSlide
    delayPoints: true
    title: "Algorithmus für 2D Heatmaps (Überblick)"
    contentWidth: parent.width*0.55
    fontScale: 0.7
    showBullets: false
    content: [
        "1. Eingabeformat einlesen",
        " Punktwolke",
        " Radius/Standardabweichung",
        "2. Geometrie erzeugen: Rechtecke",
        " Berechnung der Attribute für Rasterisierung",
        "3. 2D Wertemenge erzeugen",
        " Rasterisierung der Normalverteilungen um Datenpunkte",
        " Addition der Ergebnisse",
        "4. Transferfunktion",
        "5. Niveaulinien/Höhenlinien*"
    ]

    function advanceStep() {
        switch(currentBullet) {
//        case 5:
//            return currentStep < 2;
//        case 7:
//            return currentStep < 2;
//        case 8:
//            return currentStep < 2;
        default:
            return false;
        }
    }

    Text {
        color: "white"
        anchors.right: theSlide.right
        anchors.bottom: theSlide.bottom
        anchors.bottomMargin: theSlide.height*0.02
        text: "* hier für 8-bit Intensität"
        font.pixelSize: Math.max(theSlide.fontSize*0.3, 1.0)
        visible: theSlide.currentBullet >= 8
    }

    CodeBlock {
        id: codeblock
        z:hm2drect.z+1
        anchors.fill: parent
        anchors.bottomMargin: parent.height*0.08
        anchors.rightMargin: parent.height*0.6
        visible: theSlide.currentBullet == 5 && theSlide.currentStep == 1
        title: "raster2d.frag / GLSL"
        code:
"#version 150
in Vertex {
  flat float intensity;
  smooth vec3 toCenter;
} input;
out float out_color;

const variance = 0.2;
const float e = 2.71828182846;
const float sqrt2Pi = 2.506628275;

float normDist(float s, float var) {
  float exponent = -pow(s,2.0)/(2*pow(var,2.0));
  return (1.0/(var*sqrt2Pi))*pow(e,exponent);
}

void main() {
  float dist = length(input.toCenter);
  float intensity = normDist(dist, variance)
                     * input.intensity;
  out_color = intensity;
}"
    }

    CodeBlock {
        id: codeblockTransfer
        z:hm2drect.z+1
        anchors.fill: parent
        anchors.bottomMargin: parent.height*0.08
        anchors.rightMargin: parent.height*0.6
        visible: theSlide.currentBullet == 7 && theSlide.currentStep == 1
        title: "transfer.frag / GLSL"
        code:
"#version 150
in vec2 coord;
out vec4 out_color;
uniform sampler2D src;

vec3 hsv2rgb(vec3 c)
{
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
  float intensity = texture2D(src, coord).r;
  vec4 color = vec4( hsv2rgb(vec3(intensity, 1.0, 1.0)),
                    intensity);

  float contour = calcContour(intensity);
  out_color = mix(color, contourInputColor, contour);
}"
    }

    CodeBlock {
        id: codeblockNiveau
        z:hm2drect.z+1
        anchors.fill: parent
        anchors.bottomMargin: parent.height*0.08
        anchors.rightMargin: parent.height*0.6
        visible: theSlide.currentBullet == 8 && theSlide.currentStep == 1
        title: "transfer.frag / GLSL"
        code:
"
float getGradientLen(float intensity)
{
  float mx = dFdx(intensity);
  float my = dFdy(intensity);
  return length(vec2(mx, my));
}

float calcContour(float intensity)
{
  float scale = 1.0/contoursInterval;
  intensity *= scale;

  float gradientLen = getGradientLen(intensity);

  float halfContourWidth = contourWidth * 0.5;

  float prev = floor(intensity);
  float next = ceil(intensity);

  float distPrev = (intensity - prev) / gradientLen;
  float distNext = (next - intensity) / gradientLen;

  float isOnContourPrev = halfContourWidth - distPrev; // positive: is on contour, negative: is not on contour
  float isOnContourNext = halfContourWidth - distNext;

  float previousContourContrib = smoothstep( 0.0, 1.0, 0.75 + isOnContourPrev);
  float nextContourContrib = smoothstep(0.0, 1.0, 0.75 + isOnContourNext);

  return min(1.0, previousContourContrib + nextContourContrib);
}"
    }

    property list<HeatmapDatapointModel> pointModel: [
        HeatmapDatapointModel { centerx: 0.300; centery: 0.200 },
        HeatmapDatapointModel { centerx: 0.100; centery: 0.300; radius: 0.50 },
        HeatmapDatapointModel { centerx: 0.500; centery: 0.300 },
        HeatmapDatapointModel { centerx: 0.700; centery: 0.400; radius: 0.100 },
        HeatmapDatapointModel { centerx: 0.777; centery: 0.200; radius: 0.120 },
        HeatmapDatapointModel { centerx: 0.800; centery: 0.300; radius: 0.300 },
        HeatmapDatapointModel { centerx: 0.717; centery: 0.240; radius: 0.80 }
    ]
    property bool codeblockVisible: codeblock.visible || codeblockTransfer.visible
    Rectangle {
        id:hm2drect
        x: parent.contentWidth + (codeblockVisible?parent.width*0.13:0)
        y: parent.height*0.3
        width: (parent.width-parent.contentWidth) * (codeblockVisible?0.8:1.0)
        height: (parent.height*0.7)* (codeblockVisible?0.8:1.0)
        color: "transparent"

        Behavior on x {
            enabled: theSlide.currentBullet >= 1
            NumberAnimation { duration: 500 }
        }
        Behavior on width {
            enabled: theSlide.currentBullet >= 1
            NumberAnimation { duration: 500 }
        }
        Behavior on height {
            enabled: theSlide.currentBullet >= 1
            NumberAnimation { duration: 500 }
        }

        property color bordercol: Qt.rgba(0, 177, 172)
        property real borderrad: 20
        property real borderthick: 2
        property Gradient backgrgr: Gradient {
                                        GradientStop {
                                            position: 0.0
                                            color: Qt.rgba(0.1,0.1,0.1,1.0)
                                        }
                                        GradientStop {
                                            position: 1.0
                                            color: Qt.rgba(0.3,0.3,0.3,1.0)
                                        }
                                    }
        property real currentHM: (theSlide.currentBullet >= 2) + (theSlide.currentBullet >= 3) + (theSlide.currentBullet >= 6) + (theSlide.currentBullet >= 8) + (theSlide.currentBullet >= 9)
        Rectangle {
            radius: parent.borderrad
            border.color: parent.bordercol
            border.width: parent.borderthick
            visible: parent.currentHM >= 0
            opacity: 1.0-(parent.currentHM-0)*0.4
            gradient: parent.backgrgr
            width: parent.width*0.9
            height: parent.height*0.9
            x: -parent.currentHM*parent.width*0.1
            y: -parent.currentHM*parent.height*0.1
            Heatmap2D {
                anchors.fill: parent
                points: pointModel
                angleHoriz: 0.0
                asX: true
                visDP: false
            }
            Behavior on x {
                NumberAnimation { duration: 500 }
            }
            Behavior on y {
                NumberAnimation { duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation { duration: 500 }
            }
        }
        Rectangle {
            property int myidx: 1
            radius: parent.borderrad
            border.color: parent.bordercol
            border.width: parent.borderthick
            visible: parent.currentHM >= myidx
            opacity: (parent.currentHM >= myidx)-(parent.currentHM-myidx)*0.4
            gradient: parent.backgrgr
            width: parent.width*0.9
            height: parent.height*0.9
            x: -(parent.currentHM-myidx)*parent.width*0.1
            y: -(parent.currentHM-myidx)*parent.height*0.1
            Heatmap2D {
                anchors.fill: parent
                points: pointModel
                angleHoriz: 0.0
                asX: true
                withRad: true
                visDP: false
            }
            Behavior on x {
                //enabled: parent.currentHM >= parent.myidx
                NumberAnimation { duration: 500 }
            }
            Behavior on y {
                //enabled: parent.currentHM >= parent.myidx
                NumberAnimation { duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation { duration: 500 }
            }
        }
        Rectangle {
            property int myidx: 2
            radius: parent.borderrad
            border.color: parent.bordercol
            border.width: parent.borderthick
            visible: parent.currentHM >= myidx
            opacity: (parent.currentHM >= myidx)-(parent.currentHM-myidx)*0.4
            gradient: parent.backgrgr
            width: parent.width*0.9
            height: parent.height*0.9
            x: -(parent.currentHM-myidx)*parent.width*0.1
            y: -(parent.currentHM-myidx)*parent.height*0.1
            Heatmap2D {
                anchors.fill: parent
                points: pointModel
                angleHoriz: 0.0
                asX: false
                withRad: false
                visDP: false
                asRect: true
            }
            Behavior on x {
                //enabled: parent.currentHM >= parent.myidx
                NumberAnimation { duration: 500 }
            }
            Behavior on y {
                //enabled: parent.currentHM >= parent.myidx
                NumberAnimation { duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation { duration: 500 }
            }
        }
        Rectangle {
            property int myidx: 3
            radius: parent.borderrad
            border.color: parent.bordercol
            border.width: parent.borderthick
            visible: parent.currentHM >= myidx
            opacity: (parent.currentHM >= myidx)-(parent.currentHM-myidx)*0.4
            gradient: parent.backgrgr
            width: parent.width*0.9
            height: parent.height*0.9
            x: -(parent.currentHM-myidx)*parent.width*0.1
            y: -(parent.currentHM-myidx)*parent.height*0.1
            Heatmap2D {
                anchors.fill: parent
                points: pointModel
                angleHoriz: 0.0
                asX: false
                withRad: false
                visDP: true
                intensityFactor: 0.5
            }
            Behavior on x {
                //enabled: parent.currentHM >= parent.myidx
                NumberAnimation { duration: 500 }
            }
            Behavior on y {
                //enabled: parent.currentHM >= parent.myidx
                NumberAnimation { duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation { duration: 500 }
            }
        }
        Rectangle {
            property int myidx: 4
            radius: parent.borderrad
            border.color: parent.bordercol
            border.width: parent.borderthick
            visible: parent.currentHM >= myidx
            opacity: 1.0//(parent.currentHM >= myidx)-(parent.currentHM-myidx)*0.4
            gradient: parent.backgrgr
            width: parent.width*0.9
            height: parent.height*0.9
            x: -(parent.currentHM-myidx)*parent.width*0.1
            y: -(parent.currentHM-myidx)*parent.height*0.1
            Heatmap2D {
                anchors.fill: parent
                points: pointModel
                angleHoriz: 0.0
                asX: false
                withRad: false
                visDP: true
                colorlookup: true
                intensityFactor: 0.5
            }
            Behavior on x {
                //enabled: parent.currentHM >= parent.myidx
                NumberAnimation { duration: 500 }
            }
            Behavior on y {
                //enabled: parent.currentHM >= parent.myidx
                NumberAnimation { duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation { duration: 500 }
            }
        }
        Rectangle {
            property int myidx: 5
            radius: parent.borderrad
            border.color: parent.bordercol
            border.width: parent.borderthick
            visible: parent.currentHM >= myidx
            opacity: 1.0//(parent.currentHM >= myidx)-(parent.currentHM-myidx)*0.4
            gradient: parent.backgrgr
            width: parent.width*0.9
            height: parent.height*0.9
            x: -(parent.currentHM-myidx)*parent.width*0.1
            y: -(parent.currentHM-myidx)*parent.height*0.1
            Heatmap2D {
                anchors.fill: parent
                points: pointModel
                angleHoriz: 0.0
                asX: false
                withRad: false
                visDP: true
                colorlookup: true
                contours: true
                intensityFactor: 0.5
            }
            Behavior on x {
                //enabled: parent.currentHM >= parent.myidx
                NumberAnimation { duration: 500 }
            }
            Behavior on y {
                //enabled: parent.currentHM >= parent.myidx
                NumberAnimation { duration: 500 }
            }
            Behavior on opacity {
                NumberAnimation { duration: 500 }
            }
        }
    }
}

