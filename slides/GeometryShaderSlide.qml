import QtQuick 2.0
import ".."

SteppingSlide {
    id:slide
    title: "Geometryshader für Datenpunkte (Auszug)"
    fontScale: 0.7

    CodeBlock {
        anchors.fill: parent
        title: "datapoint.geom / GLSL"
        code:
"layout( points ) in;\n"+
"layout( triangle_strip, max_vertices = 60 ) out;\n"+
"layout(invocations = 8) in;\n"+
"\n"+
"in Centerpoint\n"+
"{\n"+
"    vec3 pos;\n"+
"    float radius;\n"+
"    float intensity;\n"+
"}input[];\n"+
"\n"+
"out Vertex\n"+
"{\n"+
"    flat float intensity;\n"+
"    smooth vec3 toCenter;\n"+
"} output;\n"+
"\n"+
"void main()\n"+
"{\n"+
"  int vertexRepeatID = gl_InvocationID;\n"+
"\n"+
"  //Berechne Layer, bringe Rechteck in korektes Koordinatensystem\n"+
"  (...)\n"+
"\n"+
"  for(int layer=startLayer ; i<endLayer ; ++layer)\n"+
"  {\n"+
"    float distLayerToCam = float(layer) * layerSize;\n"+
"    float distCenterToCenterOnLayer = distLayerToCam - centerDistToCam;\n"+
"    float radiusFactor = sqrt(1.0 - pow( distCenterToCenterOnLayer / radius, 2.0 )); //Kreisformel, Einheitskreis\n"+
"    float layerdist = distCenterToCenterOnLayer / radius;\n"+
"\n"+
"    vec4 lt, rt, lb, rb;\n"+
"    //Berechne lt, rt, lb, rb\n"+
"    (...)\n"+
"\n"+
"    output.intensity = input[0].intensity;\n"+
"    output.toCenter = vec3(radiusFactor, radiusFactor, layerdist);\n"+
"    gl_Position = projectionMatrix * lt;\n"+
"    gl_Layer = layer;\n"+
"    EmitVertex();\n"+
"    output.intensity = input[0].intensity;\n"+
"    output.toCenter = vec3(radiusFactor, -radiusFactor, layerdist);\n"+
"    gl_Position = projectionMatrix * rt;\n"+
"    gl_Layer = layer;\n"+
"    EmitVertex();\n"+
"    output.intensity = input[0].intensity;\n"+
"    output.toCenter = vec3(-radiusFactor, radiusFactor, layerdist);\n"+
"    gl_Position = projectionMatrix * lb;\n"+
"    gl_Layer = layer;\n"+
"    EmitVertex();\n"+
"    output.intensity = input[0].intensity;\n"+
"    output.toCenter = vec3(-radiusFactor, -radiusFactor, layerdist);\n"+
"    gl_Position = projectionMatrix * rb;\n"+
"    gl_Layer = layer;\n"+
"    EmitVertex();\n"+
"\n"+
"    EndPrimitive();\n"+
"  }\n"+
"}"
    }
    //Notes: äußerste Schleife parallelisieren. Bei den beiden anderen werden unsere teuren CPU-Threads zu klein...
}
