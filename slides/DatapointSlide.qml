import QtQuick 2.0
import ".."

//TODO: another heatmap3d with-> step1: addition in gemeinsamer 3D Textur; step2: Transferfunktion a) b)

SteppingSlide {
    id: hmdpSilde
    property bool showCode: true
    delayPoints: true
    title: "3D Voxelisierung"
    contentWidth: parent.width*0.55
    fontScale: 0.9
    showBullets: false
    content: [
        "Geometrische Repräsentation eines 3D Datenpunktes",
        " 1. Rechtecke erzeugen (Geometry Shader Instancing)",
        " 2. Normalverteilung um Mittelpunkt",
        " 3. 2D Rasterisierung",
        " 4. Manualle Rasterisierung der dritten Dimension durch Geometrieshader",
        "  Optimierung: Ränder (niedrige Werte) entfernen",
        " 5. Speicherung aller Punkte in einer gemeinsamen 3D Textur (Layered Rendering)"
    ]

    CodeBlock {
        id: codeblock
        z:hmdp.z+1
        anchors.fill: parent
        anchors.bottomMargin: parent.height*0.08
        anchors.rightMargin: parent.width*0.4
        visible: currentBullet == 1 && currentStep == 1
        title: "datapoint.geom / GLSL"
        code:
"layout( points ) in;\n" +
"layout( triangle_strip, max_vertices = 60 ) out;\n" +
"layout(invocations = 8) in;"
    }

    Rectangle {
        id: interpol
        z:hmdp.z+1
        anchors.fill: parent
        anchors.bottomMargin: parent.height*0.08
        anchors.rightMargin: parent.width*0.5
        visible: currentBullet == 4 && currentStep == 1
        Image {
            anchors.fill: parent
            source: "qrc:/images/interpolation.png"
            fillMode: Image.PreserveAspectFit
        }
    }
    Rectangle {
        id: bigBlackRectangle
        anchors.fill: parent
        anchors.bottomMargin: parent.height*0.01
        color: "black"
        z:hmdp.z-1
        visible: interpol.visible
    }

    Rectangle {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 5
        width: 16
        height: 16
        color: "grey"
        opacity: 0.1
        MouseArea {
            anchors.fill: parent
            onClicked: {
                resetSteps();
            }
        }
    }

    function advanceStep() {
        switch(currentBullet) {
        case 1:
            return currentStep < 1 && showCode
        case 4:
            return currentStep < 2
        case 5:
            return currentStep < 1
        default:
            return false;
        }
    }

    //0: overview:  circ: off, norm: on, zebra: off
    //1: 2d raster: circ: off, norm: off, zebra: on
    HeatmapDatapoint {
        id: hmdp
        z: 100
        anchors.right: parent.right
        anchors.margins: parent.width*0.05
        anchors.verticalCenter: parent.verticalCenter
        anchors.rightMargin: codeblock.visible?-parent.width*0.08:0

        Behavior on anchors.rightMargin {
            NumberAnimation {
                duration: 1000
                easing.type: Easing.InOutCubic
            }
        }

        width: parent.width*0.4
        height: width
        slices: 7
        angle: Math.PI*0.25
        opacity: 0.0
        Behavior on angle {
            NumberAnimation { duration: 500 }
        }
        states: [
            State {
                when: hmdpSilde.currentBullet == 0
                name: "overview"
                PropertyChanges {
                    target: hmdp
                    angle: Math.PI*0.25
                    angleHoriz: 0.5
                    colorlookup: false
                    zebra: false
                    circleAdapt: false
                    showNormDist: true
                    outerAlpha: false // todo: true test
                    sliceFrom: 0
                    sliceTo: hmdp.slices
                    opacity: 1.0
                    asRect: false
                }
                PropertyChanges {
                    target: rotationAnimation
                    running: false
                }
                PropertyChanges {
                    target: sliceAnimation
                    running: false
                }
            },
            State {
                when: hmdpSilde.currentBullet == 1
                name: "quads"
                PropertyChanges {
                    target: hmdp
                    angle: Math.PI*0.25
                    angleHoriz: 0.5
                    colorlookup: false
                    zebra: true
                    circleAdapt: false
                    showNormDist: true
                    outerAlpha: false // todo: true test
                    sliceFrom: 0
                    sliceTo: hmdp.slices
                    opacity: 1.0
                    asRect: true
                }
                PropertyChanges {
                    target: rotationAnimation
                    running: false
                }
                PropertyChanges {
                    target: sliceAnimation
                    running: false
                }
            },
            State {
                when: hmdpSilde.currentBullet == 2
                name: "normaldist"
                PropertyChanges {
                    target: hmdp
                    angle: Math.PI*0.25
                    angleHoriz: 0.5
                    colorlookup: false
                    zebra: false
                    circleAdapt: false
                    showNormDist: true
                    outerAlpha: true // todo: true test
                    sliceFrom: 0
                    sliceTo: hmdp.slices
                    opacity: 1.0
                }
                PropertyChanges {
                    target: rotationAnimation
                    running: false
                }
                PropertyChanges {
                    target: sliceAnimation
                    running: false
                }
            },
            State {
                when: hmdpSilde.currentBullet == 3
                name: "raster2d"
                PropertyChanges {
                    target: hmdp
                    angle: Math.PI*0.5
                    angleHoriz: 0
                    colorlookup: false
                    zebra: false
                    circleAdapt: false
                    showNormDist: true
                    outerAlpha: false
                    sliceFrom: hmdp.slices*0.5
                    sliceTo: hmdp.slices*0.5
                    opacity: 1.0
                }
                PropertyChanges {
                    target: rotationAnimation
                    running: false
                }
                PropertyChanges {
                    target: sliceAnimation
                    running: false
                }
            },
            State {
                when: hmdpSilde.currentBullet == 4
                name: "singlePlane3d"
                PropertyChanges {
                    target: hmdp
                    angle: Math.PI*0.25
                    angleHoriz: 0.5
                    colorlookup: false
                    zebra: false
                    showNormDist: true
                    outerAlpha: false
                    opacity: 1.0
                }
                PropertyChanges {
                    target: rotationAnimation
                    running: false
                }
            },
            State {
                when: hmdpSilde.currentBullet == 5 && hmdpSilde.currentStep == 0
                name: "zebra3d"
                PropertyChanges {
                    target: hmdp
                    angle: Math.PI*0.25
                    angleHoriz: 0.5
                    colorlookup: false
                    zebra: false
                    circleAdapt: false
                    showNormDist: true
                    outerAlpha: false
                    sliceFrom: 0
                    sliceTo: hmdp.slices
                    opacity: 1.0
                }
                PropertyChanges {
                    target: rotationAnimation
                    running: false
                }
                PropertyChanges {
                    target: sliceAnimation
                    running: false
                }
            },
            State {
                when: hmdpSilde.currentBullet == 5 && hmdpSilde.currentStep == 1
                name: "circleAdapt"
                PropertyChanges {
                    target: hmdp
                    //angle: Math.PI*0.25 //TODO: animate
                    angleHoriz: 0.5
                    colorlookup: false
                    zebra: false
                    circleAdapt: true
                    showNormDist: true
                    outerAlpha: false
                    sliceFrom: 0
                    sliceTo: hmdp.slices
                    opacity: 1.0
                }
                PropertyChanges {
                    target: rotationAnimation
                    running: true
                }
                PropertyChanges {
                    target: sliceAnimation
                    running: false
                }
            },
            State {
                when: hmdpSilde.currentBullet == 6
                name: "lowerAlpha"
                PropertyChanges {
                    target: hmdp
                    //angle: Math.PI*0.25 //TODO: animate
                    angleHoriz: 0.5
                    colorlookup: false
                    zebra: false
                    circleAdapt: true
                    showNormDist: true
                    outerAlpha: true
                    sliceFrom: 0
                    sliceTo: hmdp.slices
                    opacity: 1.0
                }
                PropertyChanges {
                    target: rotationAnimation
                    running: true
                }
                PropertyChanges {
                    target: sliceAnimation
                    running: false
                }
            },
            State {
                when: hmdpSilde.currentBullet == 7
                name: "colorlookup"
                PropertyChanges {
                    target: hmdp
                    //angle: Math.PI*0.25 //TODO: animate
                    angleHoriz: 0.5
                    colorlookup: true
                    zebra: false
                    circleAdapt: true
                    showNormDist: true
                    outerAlpha: true
                    sliceFrom: 0
                    sliceTo: hmdp.slices
                    opacity: 1.0
                }
                PropertyChanges {
                    target: rotationAnimation
                    running: true
                }
                PropertyChanges {
                    target: sliceAnimation
                    running: false
                }
            },
            State {
                when: hmdpSilde.currentBullet == 8
                name: "colorlookupHeat"
                PropertyChanges {
                    target: hmdp
                    //angle: Math.PI*0.25 //TODO: animate
                    angleHoriz: 0.5
                    colorlookup: false
                    colorlookupPost: true
                    zebra: false
                    circleAdapt: true
                    showNormDist: true
                    outerAlpha: true
                    sliceFrom: 0
                    sliceTo: hmdp.slices
                    opacity: 1.0
                }
                PropertyChanges {
                    target: rotationAnimation
                    running: true
                }
                PropertyChanges {
                    target: sliceAnimation
                    running: false
                }
            }
        ]
        transitions: [
            Transition {
                from: "normaldist"
                to: "raster2d"
                reversible: true
                ParallelAnimation {
                    NumberAnimation {
                        target: hmdp
                        property: "angle"
                        duration: 1000
                    }
                    NumberAnimation {
                        target: hmdp
                        property: "angleHoriz"
                        duration: 1000
                    }
                }
            },
            Transition {
                from: "raster2d"
                to: "*"
                reversible: false
                SequentialAnimation {
                    ParallelAnimation {
                        NumberAnimation {
                            target: hmdp
                            property: "angle"
                            duration: 1000
                        }
                        NumberAnimation {
                            target: hmdp
                            property: "angleHoriz"
                            duration: 1000
                        }
                    }
                    ParallelAnimation {
                        NumberAnimation {
                            target: hmdp
                            property: "sliceFrom"
                            duration: 5000
                            from: (hmdp.slices-1)*0.5
                            to: 0.0
                        }
                        NumberAnimation {
                            target: hmdp
                            property: "sliceTo"
                            duration: 5000
                            from: (hmdp.slices-1)*0.5
                            to: hmdp.slices-1
                        }
                    }
                }
                onRunningChanged: {
                    if(!running)
                        sliceAnimation.start();
                }
            },
            Transition {
                from: "*"
                to: "overview"
                reversible: true
                NumberAnimation {
                    target: hmdp
                    property: "opacity"
                    duration: 1000
                }
            }
        ]
        NumberAnimation {
            id: rotationAnimation
            loops: Animation.Infinite
            target: hmdp
            property: "angle"
            duration: 10000
            from: Math.PI*0.25
            to: Math.PI*1.25
        }
        ParallelAnimation {
            id:sliceAnimation
            NumberAnimation {
                loops: Animation.Infinite
                target: hmdp
                property: "sliceFrom"
                duration: 5000
                from: 0
                to: hmdp.slices
            }
            NumberAnimation {
                loops: Animation.Infinite
                target: hmdp
                property: "sliceTo"
                duration: 5000
                from: 0
                to: hmdp.slices
            }
        }
    }
}
