/****************************************************************************
**
** Copyright (C) 2012 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QML Presentation System.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/


import QtQuick 2.0
import QtQuick.Particles 2.0

Item {
    id: root
    anchors.fill: parent

    property bool showsTitle: true

    property alias sections: progInd.items
    property alias sectionHeaders: progInd.headers
    property alias currentSection: progInd.currentSection
    property alias currentItem: progInd.currentItem

    Rectangle {
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 0; color: "black" }
            GradientStop { position: 1; color: "black" }
        }
    }

    Rectangle {
        transform: Rotation { origin.x: 0; origin.y: 0; axis { x: 0; y: 0; z: 1 } angle: -90 }
        gradient: Gradient {
            GradientStop { position: 0; color: Qt.rgba(0, 177, 172) }
            GradientStop { position: 1; color: "black" }
        }
        y: parent.height * (showsTitle ? 0.1 : 0.15)
        x: 0
        height: parent.width
        width: root.height / 250
        Behavior on y {
            NumberAnimation {
                duration: 1000
                easing.type: Easing.InOutQuad
            }
        }
    }

    Rectangle {
        transform: Rotation { origin.x: 0; origin.y: 0; axis { x: 0; y: 0; z: 1 } angle: -90 }
        gradient: Gradient {
            GradientStop { position: 0; color: Qt.rgba(0, 177, 172) }
            GradientStop { position: 1; color: "black" }
        }
        y: parent.height * 0.9
        x: 0
        height: parent.width
        width: root.height / 250
    }

    Image {
        source: "qrc:/fhlogo.png"
        anchors.right: root.right
        //x: root.width-100
        y: root.height*0.1
        height: root.height*0.2
        fillMode: Image.PreserveAspectFit
    }

    ProgressIndicator {
        id: progInd
        width: thePresentation.width * 0.2
        height: thePresentation.height * 0.04
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: root.width*0.01
        opacity: thePresentation.currentSlide > 0
        //items: [[1,2,3],[1,2,3,4],[1,2,3,4,5,6,7],[1,2,3],[1],[1]]
    }
    SlideCounter {
        id: slidCount
        width: thePresentation.width * 0.2
        height: thePresentation.height * 0.07
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: root.width*0.01
        thePresentation: root.parent
    }
}
