import QtQuick 2.0
import "qtpresentation"

Slide {
    property int currentStep: 0
    property int currentBullet: 0


//    x: parent.width * 0.05
//    y: parent.height * 0.15
//    width: parent.width * 0.9
//    height: parent.height * 0.7

    function resetSteps() {
        currentStep = 0;
        _pointCounter = showAllPoints ? content.length-1 : 0;
        currentBullet = _pointCounter;
    }

    //returns false if the next slide can show
    function _advance() {
        if (!parent.allowDelay)
            return false;
        if(advanceStep()) {
            currentStep++;
            return true;
        }
        currentStep = 0;
        _pointCounter = _pointCounter + 1;
        currentBullet = _pointCounter;
        if (_pointCounter < content.length)
            return true;
        _pointCounter = showAllPoints ? content.length-1 : 0;
        currentBullet = 0;
        return false;
    }

    //returns false if the next bullet should show
    function advanceStep() {
        return false;
    }

    id:theSlide
//    MouseArea {
//        id: mouseArea
//        anchors.fill: parent
//        onClicked: {

//        }
//    }
}
