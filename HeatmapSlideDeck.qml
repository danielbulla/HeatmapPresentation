import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2

import "slides"

import "qtpresentation"

SteppingPresentation {
    id: thePresentation

    fromSlide: slideTitle
    toSlide: slideTitle

    property var sections: [
        [ slideTitle, inhaltSlide, motivationSlide, motivationSlide2/*, whatWasDoneSlide*/ ],
        [ introSlide, introSlide2/*, anforderungSlide*/ ],
        [ hm2dslide, parallelSlide, /*oglpipeSlide, hm2d1Slide,*/ niveauSlide ],//  hm2d2Slide ], //< need a break! Mehr Einordnung, roter Faden, "Sprünge" vermeiden
        [ /*sth here*/ hm3ddpslide, hm3ddpslide2, geomSlide, raycastVolumeTreeSlide, voxelSpaceSlide, physLookupCompSlide, bayerSlide ],
        [ fazitSlide, endeSlide ]
    ]

    property var sectionHeaders: [
        "Einleitung",
        "Grundlagen",
        "2D Heatmaps",
        "3D Heatmaps",
        "Ausblick"
    ]

    BackgroundFh {
        id: background
        showsTitle: currentSlide == slides.indexOf(slideTitle)
        sections: thePresentation.sections
        sectionHeaders: thePresentation.sectionHeaders
    }

    textColor: "white"

    onCurrentSlideChanged: {
        var slide = slides[currentSlide];
        //if(typeof slide == "undefined")
        //    console.log( "slide was undefined" );
        for (var index = 0; index < sections.length; ++index) {
        //    console.log( "index: "+ index + " " + sections[index])
            var ind = sections[index].indexOf( slide );
            if(ind !== -1) {
                background.currentSection = index;
                background.currentItem = ind;
                break;
            }
        }
    }

    TitleSlide {
        id: slideTitle
    }

    SteppingSlide {
        id:inhaltSlide
        title: "Inhalt"
        fontScale: 0.8
        content: [
            "Motivation",
            "Grundlagen",
            " Definition und Berechnung von Heatmaps",
            "2D Heatmaps",
            "3D Heatmaps",
            " Erzeugung",
            " Volumenrendering",
            "Fazit und Ausblick"
        ]
    }

    MotivationSlide { id: motivationSlide }

    Motivation2Slide { id: motivationSlide2 }

//    SteppingSlide {
//        id: whatWasDoneSlide
//        title: "Über diese Arbeit"
//        fontScale: 0.8
//        content: [
//            "2D Heatmaps",
//            " Ogre: Integration in Georenderer",
//            " OpenGL 3.3 Prototyp",
//            "3D Heatmaps",
//            " OpenGL 4.3 Prototyp",
//            " Integration in moderne Rendering Architektur (deferred renderer)",
//            " Algorithmen für allg. Volumenrendering",
//            "  Untersuchung div. Algorithmen",
//            "  Anpassung und Kombination geeigneter Algorithmen",
//            " Algorithmus: Punktwolke -> \"continous value\"" //nicht allgemein
//        ]
//    }
    //Anforderungen von Siemens-slide. Daten können sich jeder Zeit ändern. Löschen, hinzukommen. 3D Kamera
    IntroductionSlide { id: introSlide }

    IntroductionContSlide { id: introSlide2 }

//    SteppingSlide {
//        id: anforderungSlide
//        title: "Anforderungen"
//        fontScale: 0.9
//        content: [
//            "Echtzeit für",
//            " die Erzeugung der Definitionsmenge",
//            " Visualisierung",
//            " => Anwendung mit Heatmaps soll 30fps erreichen können (~ <30ms)",
//            "große Datenmengen",
//            " Größenordnung: 50 000 Datenpunkte", //Note: Unterschied Punktewolke. große DM: Andere Algorithmen: 5 neue Datenpunkte pro Frame. können nie mehr gelöscht werden.
//            " Daten in Echtzeit veränderbar",
//            "Effizienz",
//            " optimale Bildqualität mit gegebenen Mitteln (<30ms)"
//        ]
//    }

    SteppingSlide {
        id:intro2d
        centeredText: "2D Heatmaps"
    }
    Heatmap2DSlide {
        id:hm2dslide
    }

    CpuParallelSlide {
        id:parallelSlide
    }
//// ADDITIONAL:
//    OpenGLPipelineSlide {
//        id:oglpipeSlide
//        zoom: false
//    }
//// ADDITIONAL: INSTANCING
//    Heatmap2DStep1Slide {
//        id:hm2d1Slide
//    }
//    Heatmap2DStep2Slide {
//        id:hm2d2Slide
//    }

    // Parallelisieren:
    // 2 Möglichkeiten:
    // Eingabe: Datenpunkte. Lokale Begrenztheit.
    // Ausgabe: Berechnung pro Pixel. Für lokale Begrenztheit ist Baumstruktur notwendig.
    // (Baumstruktur: Viel Implementierungsaufwand für Industrieprojekt, Ich sehe eine Fehlerquelle durch vergessen von hinzufügen/entfernen.
    // Effiziente Baumstruktur: Fertigr Bibliothek: Siemens: Spartial Index?) (Mood: "Muss man einfach wissen, ob man diesen Weg gehen will")
    // Hybrid? -> Grafikkarte: Vertices Gleichzeitig, Fragments gleichzeitig

    // Abbildung des Algorithmus und Parallelisierung auf Grafikpipeline:
    // VertexFetch: Aus 1 mach 4. Für jeden Eingabepunkte wird ein Rechteck instanziert, welches die Fläche eingrenzt.
    // Perpektivische Verzerrung. Geometry Shader deaktiviert.
    // Interpolation der Ecken an jedem Fragement
    // Lookup Fragmentshader: Normalverteilung

    // Niveaulinien
    // Marching Squares -> CONREC (Bild)
    // Vorzeichenwechsel-> Linie
    // 1980? -> Rechenpower, um Algorithmus in Echtzeit für jeden Pixel auszuführen!
    // -> Dabei verändert sich Algorithmus an vielen Stellen. (Schnittpunkt nicht von Interesse -> Nur noch Ja, Linie oder Nein)
    // Statt VZW an 5 Punkten: Ableitung durch eine Art Sobel-Filter, den die Grafikkarte an dieser Stelle HW mäßig unterstützt. fwidth
    // Vorher Vektordaten, dazwischen wurde der Verlauf approximiert. Nun fertig gerasterte Linie
    // (Evtl. Genaigkeit ansprechen GL_FLOAT32)


    NiveauSlide {
        id:niveauSlide
    }

    SteppingSlide {
        id:intro3d
        centeredText: "3D Heatmaps"
    }

    RaycastSlide {
        id:raycastVolumeTreeSlide
    }
    VoxelSpaceSlide {
        id:voxelSpaceSlide
    }

    DatapointSlide {
        id: hm3ddpslide
        showCode: false
    }
    GeometryShaderSlide {
        id: geomSlide
    }
    Heatmap3DSlide {
        id: hm3ddpslide2
    }
    PhysicalSlide {
        id:physLookupCompSlide
    }
    BayerBlurSlide {
        id:bayerSlide
    }

    //Raycasting -> Strahl durch Volumen
    //Randnotiz zu Octrees und kd-Trees. Wozu sind diese gut geeignet?
    //VolumenRenderingIntegral
    //Terme: Licht
    //Heatmaps: Kein Phyisikalisches Phänomen welches in der realen Welt auftritt.
    // Wir müssen erst eine Physikalische Repräsentation suchen. Wie entsteht Licht bei Heatmaps.
    // 2 Modelle: Materie emmitiert farbiges Licht. Oder Materie emmitiert Strahlung einer einzigen Wellenlänge, unterschiedlicher Intensität.

    SteppingSlide {
        id:fazitSlide
        title: "Fazit und Ausblick"
        fontScale: 0.8
        content: [
            "2D Heatmaps produktiv",
            "3D Heatmaps",
            " Machbarkeit gezeigt",
            " # Punkte erhöhen",
            " \"pre integrated transfer function\""
        ]
        Image {
            id: preIntImg
            source: "qrc:/images/pre-integrated.jpg"
            anchors.right: parent.right
            anchors.bottom: captionPrI.top
            width: parent.width * 0.5
            fillMode: Image.PreserveAspectFit
        }
        Text {
            id:captionPrI
            color: "white"
            font.pixelSize: parent.height*0.025
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.height*0.01
            anchors.left: preIntImg.left
            anchors.right: preIntImg.right
            text: "Quelle: Klaus Engel, informatik.uni-stuttgart.de"
            horizontalAlignment: Text.AlignHCenter
        }
    }

    SteppingSlide {
        id: endeSlide
        title: "Fragen?"
        centeredText: "Vielen Dank"
        Text {
            color: "white"
            font.pixelSize: parent.height*0.04
            text: "Daniel Bulla\nMatr.Nr.:826708\ndbulla@fh-aachen.de"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: parent.height*0.1
        }
        Rectangle {
            id: clipRect
            z: imgGrid.z+1
            gradient: Gradient {
                GradientStop { position: 0.0; color: "black" }
                GradientStop { position: 0.1; color: "transparent" }
                GradientStop { position: 0.9; color: "transparent" }
                GradientStop { position: 1.0; color: "black" }
            }
            rotation: 90
            height: parent.width
            width: parent.height*0.98
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
        }
        Image {
            id:fullscreenImage
            z:imgGrid.z+100
            anchors.fill: parent
            anchors.bottomMargin: parent.height*0.01
            fillMode: Image.PreserveAspectFit
            mipmap: true
            smooth: true
            visible: false
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    parent.visible = !parent.visible;
                }
            }
            function show(src) {
                source = src;
                visible = true;
                showAnimation.start();
            }

            NumberAnimation {
                id: showAnimation
                target: fullscreenImage
                property: "opacity"
                duration: 500
                easing.type: Easing.InOutQuad
                from: 0.0
                to: 1.0
            }
        }
        Grid {
            id: imgGrid
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.height*0.01+parent.height*0.15
            height: parent.height*0.15
            width: parent.width*0.99
            columns: 8
            rows: 2
            property real normalImageWidth: width*0.12
            property bool imgMip: true
            property bool imgSmooth: true
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/contouring.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/darkening.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/hmtitle.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/outsideblob.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/Heatmap-20141012linewidthcorrectionoff2.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/Heatmap-20141012linewidthcorrectionon2.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/deferred.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/Heatmapred.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/bayer-on.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/bayer-off.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/deferredrendering.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/set5-freeinsponza.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/hull2.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/softparticlesoff2_rect.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/softparticleson2_rect.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
            Image {
                height: parent.height
                width: parent.normalImageWidth
                source: "qrc:/images/set5-free2.png"
                fillMode: Image.PreserveAspectCrop
                mipmap: parent.imgMip
                smooth: parent.imgSmooth
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        fullscreenImage.show(parent.source);
                    }
                }
            }
        }
    }
}
