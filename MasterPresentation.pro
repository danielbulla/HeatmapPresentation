TEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp

RESOURCES += qml.qrc \
    part1.qrc \
    part2.qrc \
    part3.qrc \
    part4.qrc \
    part5.qrc \
    part6.qrc \
    part1.qrc \
    part2.qrc \
    part3.qrc \
    part4.qrc \
    part5.qrc \
    part6.qrc

#QMAKE_CXXFLAGS -= -Zm200
#QMAKE_CXXFLAGS += -Zm2000

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
