import QtQuick 2.0

Item {
    property SteppingPresentation thePresentation
    property int rattenSchwanz: 0
    Text {
        color: "white"
        text: (thePresentation.currentSlide + 1) + " / " + (thePresentation.slides.length - rattenSchwanz)
        font.pixelSize: 16
        opacity: thePresentation.currentSlide >= 1 && (thePresentation.currentSlide + 1) <= (thePresentation.slides.length - rattenSchwanz)
        Behavior on opacity {
            NumberAnimation { duration: 500 }
        }
    }
}

